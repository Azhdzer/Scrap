const fs = require('fs');
const express = require('express');
const mongoose = require('mongoose');
const puppeteer = require('puppeteer');
const cheerio = require('cheerio');
const axios = require('axios');

// Подключение к базе данных MongoDB
const db = 'mongodb+srv://Artsiom:future76@cluster0.n5fxsnv.mongodb.net/';

// Устанавливаем соединение с базой данных MongoDB
mongoose.connect(db, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => {
    console.log('Успешное подключение к MongoDB');
    // Запуск парсинга данных
    parseData();
  })
  .catch((error) => {
    console.error('Ошибка при подключении к MongoDB:', error);
  });

// Модель статьи MongoDB
const Article = mongoose.model('Article', {
  title: String,
  description: String,
  category: String,
});

// Функция для парсинга данных
async function parseData() {
  const link = 'https://www.bialystok.pl/pl/dla_turystow/turystyczne_praktyczne_info/turystyczne-aktualnosci.html?page=';
  let flag = true;
  let res = [];
  let counter = 1;

  try {
    // Запуск браузера Puppeteer
    const browser = await puppeteer.launch({
      headless: false,
      slowMo: 100,
      devtools: true,
    });
    const page = await browser.newPage();
    await page.setViewport({ width: 1400, height: 900 });

    while (flag) {
      // Переход на страницу
      await page.goto(`${link}${counter}`);
      await page.waitForSelector('a[aria-label="Aktualnie wybrana strona"]');
      console.log('page: ', counter);

      // Извлечение данных со страницы
      let html = await page.evaluate(async () => {
        const page = [];

        try {
          const divs = document.querySelectorAll('li[data-url]');

          console.log(divs);

          divs.forEach((div) => {
            const a = div.querySelector('h2.font-fluid');

            const obj = {
              title: a !== null ? a.innerText : 'NO-TITLE',
              description: div.querySelector('p.font-fluid').innerText,
              category: div.querySelector('div.tags') !== null ? div.querySelector('div.tags').innerText.replace(/\n/g, ' ') : 'NO-CATEGORY',
            };

            page.push(obj);
          });
        } catch (e) {
          console.log(e);
        }

        return page;
      }, { waitUntil: 'networkidle2' });

      res.push(html);

      // Проверяем условие остановки цикла
      for (let i in res) {
        if (res[i].length === 0) { flag = false; }
      }

      counter++;
    }

    await browser.close();

    // Сохранение данных в файл 
    res = res.flat();

    fs.writeFile('bialystok.json', JSON.stringify({ data: res }), (err) => {
      if (err) throw err;
      console.log('bialystok.json saved');
      console.log('bialystok.json length - ', res.length);

      // Сохранение данных в базу данных
      saveToDatabase();
    });

    console.log('end');
  } catch (e) {
    console.log(e);
  }
}

// Функция для сохранения данных в базу данных
function saveToDatabase() {
  // Чтение данных из файла bialystok.json
  fs.readFile('bialystok.json', 'utf8', (err, data) => {
    if (err) {
      console.error('Ошибка чтения файла bialystok.json:', err);
      return;
    }

    // Преобразование данных в JSON формат
    const jsonData = JSON.parse(data);

    // Сохранение данных в базу данных MongoDB
    Article.insertMany(jsonData.data)
      .then(() => {
        console.log('Данные успешно сохранены в базу данных');

        // Запуск сервера Express.js
        startServer();
      })
      .catch((error) => {
        console.error('Ошибка при сохранении данных в базу данных:', error);
      });
  });
}

// Функция для запуска сервера Express.js
function startServer() {
  const app = express();
  const port = 3000;

  app.use(express.json());

  // Обработка POST-запроса и выполнение скрапинга
  app.post('/articles', async (req, res) => {
    try {
      const { url } = req.body;

      const response = await axios.get(url);
      const html = response.data;

      const $ = cheerio.load(html);
      const title = $('h1').text();
      const description = $('p').text();
      const category = $('div').text(); 

      if (!title || !description || !category) {
        throw new Error('Некорректные данные статьи');
      }

      const article = new Article({ title, description, category });

      await article.save();

      res.status(200).json({ message: 'Статья сохранена' });
    } catch (error) {
      console.error('Ошибка при сохранении статьи:', error);
      res.status(500).json({ error: 'Ошибка при сохранении статьи' });
    }
  });

  // Обработка DELETE-запроса и удаление статьи
  app.delete('/articles/:id', async (req, res) => {
    try {
      const { id } = req.params;

      await Article.findByIdAndDelete(id);

      res.status(200).json({ message: 'Статья удалена' });
    } catch (error) {
      console.error('Ошибка при удалении статьи:', error);
      res.status(500).json({ error: 'Ошибка при удалении статьи' });
    }
  });

  app.listen(port, () => {
    console.log(`Сервер запущен на порту ${port}`);
  });
}
